<?php
declare (strict_types = 1);

namespace app\api\validate;

use think\Validate;

class Modular extends Validate
{
    /**
     * @param $id
     * 验证条件
     * @return array
     */
    public static function rules($id){
        $str = '';
        if(0 !== $id){
            $str = ',' . $id;
        }
        return [
            'title' => 'require|min:2|max:25|unique:modular,title'.$str,
            'path' => 'unique:modular,path'.$str,
        ];
    }

    /**
     * @return array
     * 错误信息
     */
    public static function msg()
    {
        return [
            'title.require' => '模块名称不能为空',
            'title.unique'=>'模块名称已存在',
            'title.min' =>'模块名称最小长度不能少于2个字符',
            'title.max' =>'模块名称长度不能超过25字符',
            'path.unique'=>'模块路由已存在',

        ];

    }
}
