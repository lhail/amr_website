<?php
declare (strict_types = 1);

namespace app\api\validate;

use think\Validate;

class PhotoAlbum extends Validate
{
    /**
     * @param $id
     * 验证条件
     * @return array
     */
    public static function rules($id){
        $str = '';
        if(0 !== $id){
            $str = ',' . $id;
        }
        return [
            'title' => 'require|min:2|max:118',
            'column_id' => 'require',
        ];
    }

    /**
     * @return array
     * 错误信息
     */
    public static function msg()
    {
        return [
            'title.require' => '相册名称不能为空',
            'title.min' =>'相册名称最小长度不能少于2个字符',
            'title.max' =>'相册名称长度不能超过118字符',
            'column_id.require' => '相册栏目不能为空',
        ];

    }
}
