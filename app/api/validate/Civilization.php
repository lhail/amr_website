<?php
declare (strict_types = 1);

namespace app\api\validate;

use think\Validate;

class Civilization extends Validate
{
    /**
     * @param $id
     * 验证条件
     * @return array
     */
    public static function rules($id){
        $str = '';
        if(0 !== $id){
            $str = ',' . $id;
        }
        return [
            'column_id' => 'require|unique:civilization,column_id'.$str,
        ];
    }

    /**
     * @return array
     * 错误信息
     */
    public static function msg()
    {
        return [
            'column_id.require' => '请选择栏目',
            'column_id.unique'=>'栏目已经存在',
        ];

    }
}
