<?php
declare (strict_types = 1);

namespace app\api\validate;

use think\Validate;

class Article extends Validate
{
    /**
     * @param $id
     * 验证条件
     * @return array
     */
    public static function rules($id){
        $str = '';
        if(0 !== $id){
            $str = ',' . $id;
        }
        return [
            'title' => 'require|min:2|max:25|unique:article,title'.$str,
        ];
    }

    /**
     * @return array
     * 错误信息
     */
    public static function msg()
    {
        return [
            'title.require' => '文章名称不能为空',
            'title.unique'=>'文章名称已存在',
            'title.min' =>'文章名称最小长度不能少于2个字符',
            'title.max' =>'文章名称长度不能超过25字符',
        ];

    }
}
