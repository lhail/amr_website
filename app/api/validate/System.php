<?php
declare (strict_types = 1);

namespace app\api\validate;

use think\Validate;

class System extends Validate
{
    /**
     * @param $id
     * 验证条件
     * @return array
     */
    public static function rules($id){
        $str = '';
        if(0 !== $id){
            $str = ',' . $id;
        }
        return [
            'web_site_address' => 'require|unique:system_set_up,web_site_address^web_site_hotline^web_site_time_slot^web_site_icp^web_site_copy'.$str,
//            'web_site_address' => 'require|unique:system_set_up,account,'.$str.',web_site_address',
//            'web_site_hotline' => 'require|unique:system_set_up,web_site_address,'.$str,
//            'web_site_time_slot' => 'require|unique:system_set_up,account,'.$str,
//            'web_site_icp' => 'require|unique:system_set_up,account,'.$str,
//            'web_site_copy' => 'require|unique:system_set_up,account,'.$str,
        ];
    }

    /**
     * @return array
     * 错误信息
     */
    public static function msg()
    {
        return [
            'web_site_address.require'=>'网站地址不能为空',
            'web_site_hotline.require' => '服务热线不能为空',
            'web_site_time_slot.require'=>'服务热线时间段不能为空',
            'web_site_icp.require' => '网站备案号不能为空',
               'web_site_copy.require'=>'版权信息不能为空',
        ];

    }
}
