<?php
declare (strict_types = 1);

namespace app\api\validate;

use think\Validate;

class Login extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'mobile' => 'require|mobile|regex:/^1[34578][0-9]{9}$/',
        'password' => 'require|min:6|max:20',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'mobile.require' => '账号不能为空',
        'mobile.regex' => '账号必须为手机号码',
        'password.require' => '密码不能为空',
        'password.min' => '密码长度不能小于6',
        'password.max' => '密码长度不能大于20',
    ];
}
