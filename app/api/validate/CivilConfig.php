<?php
declare (strict_types = 1);

namespace app\api\validate;

use think\Validate;

class CivilConfig extends Validate
{
    /**
     * @param $id
     * 验证条件
     * @return array
     */
    public static function rules($id){
        $str = '';
        if(0 !== $id){
            $str = ',' . $id;
        }
        return [
            'title' => 'require|max:20|unique:civil_config,title'.$str,
        ];
    }

    /**
     * @return array
     * 错误信息
     */
    public static function msg()
    {
        return [
            'title.require' => '名称不能为空',
            'title.unique'=>'名称已存在',
            'title.max' =>'名称字符长度不能超过20',
        ];

    }
}
