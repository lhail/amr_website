<?php
declare (strict_types = 1);

namespace app\api\validate;

use think\Validate;

class Cooperative extends Validate
{
    /**
     * @param $id
     * 验证条件
     * @return array
     */
    public static function rules($id){
        $str = '';
        if(0 !== $id){
            $str = ',' . $id;
        }
        return [
            'name' => 'require|min:2|max:8|unique:cooperative,name'.$str,
        ];
    }

    /**
     * @return array
     * 错误信息
     */
    public static function msg()
    {
        return [
            'name.require' => '模块名称不能为空',
            'name.unique'=>'模块名称已存在',
            'name.min' =>'模块名称最小长度不能少于2个字符',
            'name.max' =>'模块名称长度不能超过8字符',

        ];

    }
}
