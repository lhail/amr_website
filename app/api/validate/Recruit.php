<?php
declare (strict_types = 1);

namespace app\api\validate;

use think\Validate;

class Recruit extends Validate
{
    /**
     * @param $id
     * 验证条件
     * @return array
     */
    public static function rules($id){
        $str = '';
        if(0 !== $id){
            $str = ',' . $id;
        }
        return [
            'name' => 'require|unique:recruit,name'.$str,
        ];
    }

    /**
     * @return array
     * 错误信息
     */
    public static function msg()
    {
        return [
            'name.require' => '职位不能为空',
            'name.unique'=>'职位已经存在',
        ];

    }
}
