<?php
declare (strict_types = 1);

namespace app\api\model;

use think\Model;

/**
 * @mixin \think\Model
 */
class Market extends AppModel
{
    //
    //
    //类型
    const TYPE_ONE = 1;
    const TYPE_TWO = 2;
    const TYPE_THREE = 3;
    const TYPE_TOUR = 4;

    /**
     * @var string[]
     * @User: 刘海龙
     * @Date: 2021/3/5
     * @Time: 11:06
     * 后台类别
     */
    public static $is_type = array(
        self::TYPE_ONE => '商圈研究',
        self::TYPE_TWO => '消费者研究',
        self::TYPE_THREE => '产品定位研究',
        self::TYPE_TOUR => '广告媒体研究',
    );
}
