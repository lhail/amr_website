<?php
declare (strict_types = 1);

namespace app\api\model;

use think\Model;

/**
 * @mixin \think\Model
 */
class Civilization extends AppModel
{
    //
    //
    //类型
    const TYPE_ONE = 1;
    const TYPE_TWO = 2;

    /**
     * @var string[]
     * @User: 刘海龙
     * @Date: 2021/3/5
     * @Time: 11:06
     * 后台类别
     */
    public static $is_type = array(
        self::TYPE_ONE => '文明眼创城动态管理平台',
        self::TYPE_TWO => '新时代文明实践中心云管理平台',
    );

    public function CivilConfig(){
        return $this->hasMany(CivilConfig::class,'ci_id');
    }
}
