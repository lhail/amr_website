<?php
declare (strict_types = 1);

namespace app\api\model;

use think\Model;

/**
 * @mixin \think\Model
 */
class Users extends AppModel
{
    protected $hidden = ['password'];

    /**
     * @User: 刘海龙
     * @Date: 2021/2/25
     * @Time: 15:15
     * @return \think\model\relation\BelongsToMany
     * 多对多
     */
    public function roles()
    {
        return $this->belongsToMany(AuthGroup::class,'auth_group_access','group_id','uid');
    }

    /**
     * @param $password
     * @User: 刘海龙
     * @Date: 2021/2/25
     * @Time: 14:56
     * @return string
     * 密码md5之后sha1加密
     */
    public static function makePassword($password)
    {
        return md5(sha1($password));
    }

    /**
     * @param $input_md5_password
     * @User: 刘海龙
     * @Date: 2021/2/25
     * @Time: 14:56
     * @return array|Model|null
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 验证密码
     */
    public function checkHashPassword($input_md5_password)
    {
        $check = md5(sha1($input_md5_password));
        return self::where('password', $check)->find();
    }
}
