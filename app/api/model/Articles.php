<?php
declare (strict_types = 1);

namespace app\api\model;

/**
 * @mixin \think\Model
 */
class Articles extends AppModel
{
    protected $name = 'article';
    //菜单类型
    const TYPE_ONE = 1;
    const TYPE_TWO = 2;

    /**
     * @var string[]
     * @User: 刘海龙
     * @Date: 2021/3/5
     * @Time: 11:06
     * 后台类别
     */
    public static $is_type = array(
        self::TYPE_ONE => '行业咨讯',
        self::TYPE_TWO => '公司动态',
    );

    /**
     * @var string[]
     * @User: 刘海龙
     * @Date: 2021/3/5
     * @Time: 11:06
     * 根据左右类别前端读取信息
     */
    public static $is_column = array(
        self::TYPE_ONE => 'left',
        self::TYPE_TWO => 'right',
    );
}
