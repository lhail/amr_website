<?php
declare (strict_types = 1);

namespace app\api\model;

use think\Model;

/**
 * @mixin \think\Model
 */
class CivilConfig extends AppModel
{
    //
    //
    //
    //类型
    const TYPE_ONE = 1;
    const TYPE_TWO = 2;
    const TYPE_THREE = 3;

    /**
     * @var string[]
     * @User: 刘海龙
     * @Date: 2021/3/5
     * @Time: 11:06
     * 后台类别
     */
    public static $is_type = array(
        self::TYPE_ONE => '内容展示',
        self::TYPE_TWO => '案例展示',
        self::TYPE_THREE => '解决问题',
    );
}
