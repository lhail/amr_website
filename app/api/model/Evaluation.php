<?php
declare (strict_types = 1);

namespace app\api\model;

use think\Model;

/**
 * @mixin \think\Model
 */
class Evaluation extends AppModel
{
    //
    //类型
    const TYPE_ONE = 1;
    const TYPE_TWO = 2;

    /**
     * @var string[]
     * @User: 刘海龙
     * @Date: 2021/3/5
     * @Time: 11:06
     * 后台类别
     */
    public static $is_type = array(
        self::TYPE_ONE => '神秘顾客',
        self::TYPE_TWO => '客户满意度',
    );
}
