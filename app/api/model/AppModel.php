<?php
declare (strict_types = 1);

namespace app\api\model;

use think\Model;
use think\model\concern\SoftDelete;

/**
 * @mixin \think\Model
 */
class AppModel extends Model
{
    use SoftDelete;
}
