<?php


namespace app\api\model;


use think\model\Pivot;

class GroupUserPivot extends Pivot
{
    protected $name = 'auth_group_access';



    public static function roleUserSave($user,$role_id)
    {
        $role_id = explode(',',$role_id);
        $user->roles()->detach();
        $user->roles()->attach($role_id);
    }
}