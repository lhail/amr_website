<?php
declare (strict_types = 1);

namespace app\api\model;

class Carousel extends AppModel
{
    //


    public function navigation(){
        return $this->belongsTo(Navigation::class,'nav_id');
    }
}
