<?php
declare (strict_types = 1);

namespace app\api\model;

class PhotoAlbum extends AppModel
{

    public function navigation(){
        return $this->belongsTo(Navigation::class,'column_id');
    }
}
