<?php


use think\facade\Route;
//公共路由
Route::post("login", 'Login/userLogin');
//后台路由
Route::group('dashboard',function (){
    //获取用户信息
    Route::get('user_show', 'Login/userShow');
    //获取菜单权限
    Route::get('permission', 'Menu/menuPermission');
    //用户列表
    Route::get('user_list', 'User/userList');
    //用户更新
    Route::get('update_show/:id', 'User/userUpdateShow');
    //用户保存
    Route::post('user_save', 'User/userSave');
    //删除用户
    Route::get('user_delete/:id', 'User/userDelete');
    //上传用户头像
    Route::post('upload_head_img', 'User/uploadHeadImg');
    //获取角色列表 代树
    Route::get('role_list', 'Role/roleList');
    //读取所有角色
    Route::get('role_info_list', 'Role/roleInfoList');
    //保存中间表角色
    Route::post('set_role_user_pivot', 'Role/setRoleUserPivot');
    //新增and编辑角色
    Route::post('save_role', 'Role/saveRole');
    //更改角色状态
    Route::put('update_role_status/:id/:status', 'Role/updateRoleStatus');
    //删除角色
    Route::delete('del_role/:id/:role_id', 'Role/delRole');
    //删除角色组
    Route::delete('del_group/:id', 'Role/groupDel');
    //菜单
    Route::get('menu_list', 'Menu/MenuInfo');
    //新增and编辑
    Route::post('menuSave', 'Menu/MenuSave');
    //删除菜单
    Route::get('menuDel/:id', 'Menu/MenuDel');
    //添加子菜单
    Route::post('child_save', 'Menu/childSave');
    //获取菜单类别
    Route::get('menuType', 'Menu/MenuType');
    //上传菜单图标
    Route::post('upload_icon', 'Menu/UploadIcon');
    //导航栏数据列表
    Route::get('nav_list','Nav/navList');
    //保存导航栏
    Route::post('save_nav','Nav/saveNavigation');
    //删除导航
    Route::delete('del_nav/:id','Nav/delNav');
    //获取新闻类别
    Route::get('article_type_list','Article/articleTypeList');
    //获取新闻列表
    Route::get('article_list','Article/ArticleList');
    //新增文章
    Route::post('article_save','Article/saveArticle');
    //读取单条文章信息
    Route::get('article_show/:id','Article/ArticleShow');
    //删除文章
    Route::delete('del_article/:id','Article/delArticle');
    //banner列表
    Route::get('banner_list','Banner/bannerList');
    //保存Banner
    Route::post('save_banner','Banner/saveBanner');
    //获取信息
    Route::get('show_banner/:id','Banner/showBanner');
    //删除banner
    Route::delete('del_banner/:id','Banner/delBanner');
    //获取模块列表
    Route::get('modular_list','SystemConfig/ModularList');
    //保存首页模块
    Route::post('modular_save','SystemConfig/saveModular');
    //模块展示
    Route::get('modular_show/:id','SystemConfig/ModularShow');
    //删除模块
    Route::delete('del_modular/:id','SystemConfig/delModular');
    //列表合作客户
    Route::get('cooper_list','SystemConfig/CooperList');
    //保存合作客户
    Route::post('save_cooper','SystemConfig/saveCooper');
    //获取单一信息
    Route::get('show_cooper/:id','SystemConfig/showCooper');
    //删除合作客户
    Route::delete('del_cooper/:id','SystemConfig/delCooper');
    //查询
    Route::get('introduce_list','Company/IntroduceList');
    //保存公司简介
    Route::post('save_introduce','Company/saveIntroduce');
    //文化查询列表
    Route::get('culture_list','Company/CultureList');
    //保存企业文化
    Route::post('save_culture','Company/saveCulture');
    //相册列表
    Route::get('photo_list','Company/PhotoList');
    //保存相册
    Route::post('save_photo','Company/savePhoto');
    //查看相册
    Route::get('show_photo/:id','Company/showPhoto');
    //删除相册
    Route::delete('del_photo/:id','Company/delPhoto');
    //第三方测评
    //列表
    Route::get('assess_list','Server/assessList');
    //保存
    Route::post('save_assess','Server/saveAssess');
    //服务质量
    //获取服务质量类别
    Route::get('evaluation_type_list','Server/evaluationTypeList');
    //列表
    Route::get('evaluation_list','Server/evaluationList');
    //保存
    Route::post('save_evaluation','Server/saveEvaluation');
    //获取唯一信息
    Route::get('show_eval/:id','Server/showEvaluation');
    //删除
    Route::delete('del_eval/:id','Server/delEvaluation');

    //文明眼
    //列表
    Route::get('civilization_list','Server/CivilizationAdminList');
    //保存
    Route::post('save_civilization','Server/saveCivilization');
    //获取类别
    Route::get('civilization_type_list','Server/civilizationTypeList');
    //获取唯一信息
    Route::get('show_civil/:id','Server/showCivilization');
    Route::get('show_civil_config/:id','Server/showCivilizationConfig');
    //删除
    Route::delete('del_civil/:id','Server/delCivil');
    //删除配置
    Route::delete('del_civil_config/:id','Server/delCivilConfig');
    //配置
    Route::post('save_civil_config','Server/saveCivilConfig');
    //获取类别
    Route::get('civil_type_list','Server/civilConfigTypeList');

    //市场调研
    //列表
    Route::get('market_list','Server/MarketList');
    //市场调研类别
    Route::get('market_type_list','Server/marketTypeList');
    //保存
    Route::post('save_market','Server/saveMarket');
    //获取唯一信息
    Route::get('show_market/:id','Server/showMarket');
    //删除
    Route::delete('del_market/:id','Server/delMarket');

    //招聘
    Route::get('recruit_list','Recruit/getRecruitList');
    //获取唯一信息
    Route::get('show_recruit/:id','Recruit/showRecruit');
    //保存招聘信息
    Route::post('save_recruit','Recruit/saveRecruit');
    //删除
    Route::delete('del_recruit/:id','Server/delRecruit');

    //系统设置
    //查询
    Route::get('system_list','SystemConfig/getSystemList');
    //保存
    Route::post('save_system','SystemConfig/saveSystemSetUp');

    //上传图片
    Route::post('upload_place_img', 'Article/uploadImg');


})->middleware(\app\middleware\Authenticate::class);

//前台路由
Route::group('home',function (){
    //获取导航菜单
    Route::get('nav_list','Nav/navList');
    //根据菜单路由获取banner
    Route::post('show_banner_list','Banner/showBannerList');
    //获取首页模块
    Route::get('modular_list','SystemConfig/ModularList');
    //获取新闻
    Route::get('home_article_list','Article/homeArticleList');
    //合作客户列表
    Route::get('cooper_list','SystemConfig/CooperList');
    //获取所有图片
    Route::get('cooper_all_list','SystemConfig/CooperListAll');
    //获取新闻类别
    Route::get('article_type_list','Article/articleTypeList');
    //获取新闻列表
    Route::get('article_list','Article/ArticleList');
    //相册列表
    Route::get('photo_list','Company/PhotoList');
    //第三方测评
    Route::get('assess_list','Server/assessList');
    //公司简介
    Route::get('introduce_list','Company/IntroduceList');
    //文化查询列表
    Route::get('culture_list','Company/CultureList');
    //获取服务质量类别
    Route::get('evaluation_type_list','Server/evaluationTypeList');
    //列表
    Route::get('evaluation_list','Server/evaluationList');
    //获取类别
    Route::get('civilization_type_list','Server/civilizationTypeList');
    //列表
    Route::get('civilization_list','Server/CivilizationHomeList');
    //获取案例
    Route::get('get_case_list','Server/getCaseList');
    //解决问题
    Route::get('solve_the_problem','Server/solveTheProblem');
    //市场调研
    //列表
    Route::get('market_list','Server/MarketList');
    //市场调研类别
    Route::get('market_type_list','Server/marketTypeList');
    //招聘
    Route::get('recruit_list','Recruit/getRecruitList');
    //新闻详情页
    Route::get('show_article/:id','Article/showArticle');
    //查询
    Route::get('system_list','SystemConfig/getSystemList');

});




