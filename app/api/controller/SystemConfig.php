<?php
declare (strict_types = 1);

namespace app\api\controller;

use app\api\helpers\traits\SearchDataForModel;
use app\api\model\Cooperative;
use app\api\model\Modular;
use app\api\model\SystemSetUp;
use think\exception\ValidateException;
use think\Request;

class SystemConfig extends BaseController
{
    use SearchDataForModel;

    /** 首页模块 */
    /**
     * @User: 刘海龙
     * @Date: 2021/3/5
     * @Time: 9:32
     * @return \think\response\Json
     * 获取模块列表
     */
    public function ModularList(){
        $limit = $this->request->param('limit');
        $map = [];
        if ($this->request->has('title') && !empty($this->request->param('title'))) {
            $map[] = ['title', 'like', '%' . $this->request->param('title') . '%'];
        }
        $data = $this->search(new Modular(),$map,$limit,'asc');
        return $this->response($data);
    }

    /**
     * @param $id
     * @User: 刘海龙
     * @Date: 2021/3/5
     * @Time: 9:37
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 模块展示
     */
    public function ModularShow($id){
        $data = Modular::find($id)->toArray();
        if (is_null($data)) {
            return $this->response(404, '此模块不存在');
        }
        //拼接图片显示数组
        $data['status'] = $data['status'] == 1 ? true : false;
        return $this->response($data);
    }

    /**
     * @User: 刘海龙
     * @Date: 2021/3/5
     * @Time: 9:20
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 保存模块
     */
    public function saveModular(){

        $id = $this->request->param('id',0);
        try {
            $validator = $this->validate($this->request->param(),
               \app\api\validate\Modular::rules($id),
                \app\api\validate\Modular::msg());
            if ($validator){
                $data = $this->request->param();
                $data['create_id'] = $this->request->uid;
                $data['status'] = $this->request->param('status', true) ? 1 : 0;
                if (intval($id) === 0) {
                    Modular::create($data);
                } else {
                    $modular = Modular::find($id);
                    $modular->save($data);
                }
                return $this->response();
            }
        }catch (ValidateException $exception) {
            return $this->response(403, $exception->getError());
        }
    }

    /**
     * @param $id
     * @User: 刘海龙
     * @Date: 2021/3/5
     * @Time: 9:49
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 删除模块
     */
    public function delModular($id){
        $ids = explode(',', $id);
        foreach ($ids as $id) {
            $modular = Modular::find($id);
            if (is_null($modular)) {
                return $this->response(404, '此模块不存在');
            }
            $modular->delete();
        }
        return $this->response();
    }

    /** 合作客户 */

    /**
     * @User: 刘海龙
     * @Date: 2021/3/5
     * @Time: 14:16
     * @return \think\response\Json
     * @throws \think\db\exception\DbException
     * 案例客户读取
     */
    public function CooperList(){
        $limit = $this->request->param('limit');
        $map = [];
        if ($this->request->has('name') && !empty($this->request->param('name'))) {
            $map[] = ['name', 'like', '%' . $this->request->param('name') . '%'];
        }
        $data = $this->search(new Cooperative(),$map,$limit,'desc')->each(function ($item,$key){
            $img_arr = json_decode($item['img'],true);
            $item['url']  = array_shift($img_arr) ?? '';
        });
        return $this->response($data);
    }

    /**
     * @User: 刘海龙
     * @Date: 2021/3/5
     * @Time: 17:34
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 获取所有图片集合
     */
    public function CooperListAll(){
        $data = Cooperative::field('img')->select()->toArray();
        $srcList = [];
        foreach ($data as $key=>$v){
            $img_arr = json_decode($v['img'],true);
            $img_arr  = array_shift($img_arr) ?? '';
            $srcList[] = $img_arr['url'] ?? '';
        }
        return $this->response($srcList);
    }

    /**
     * @param $id
     * @User: 刘海龙
     * @Date: 2021/3/5
     * @Time: 14:42
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 获取单一信息
     */
    public function showCooper($id){
        $data = Cooperative::find($id)->toArray();
        if (is_null($data)) {
            return $this->response(404, '不存在');
        }
        $data['img'] = json_decode($data['img'], true);
        $data['status'] = $data['status'] == 1 ? true : false;
        return $this->response($data);
    }

    /**
     * @param $id
     * @User: 刘海龙
     * @Date: 2021/3/5
     * @Time: 14:48
     * @return \think\response\Json
     * 删除合作客户信息
     */
    public function delCooper($id){
        $ids = explode(',', $id);
        foreach ($ids as $id) {
            $cooper = Cooperative::find($id);
            if (is_null($cooper)) {
                return $this->response(404, '此合作客户不存在');
            }
            $cooper->delete();
        }
        return $this->response();
    }

    /**
     * @User: 刘海龙
     * @Date: 2021/3/5
     * @Time: 14:09
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 保存合作客户
     */
    public function saveCooper(){
        $id = $this->request->param('id',0);
        try {
            $validator = $this->validate($this->request->param(),
                \app\api\validate\Cooperative::rules($id),
                \app\api\validate\Cooperative::msg());
            if ($validator){
                $data = $this->request->param();
                $data['img'] = json_encode($data['img']);
                $data['create_id'] = $this->request->uid;
                $data['status'] = $this->request->param('status', true) ? 1 : 0;
                if (intval($id) === 0) {
                    Cooperative::create($data);
                } else {
                    $cooper = Cooperative::find($id);
                    $cooper->save($data);
                }
                return $this->response();
            }
        }catch (ValidateException $exception) {
            return $this->response(403, $exception->getError());
        }
    }

    /**
     * 系统设置
     */

    /**
     * @User: 刘海龙
     * @Date: 2021/3/17
     * @Time: 11:16
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 数据读取
     */
    public function getSystemList(){
        $id = $this->request->param('id');
        $data = SystemSetUp::find($id);
        $data['moreNotifyObject'] = json_decode($data['web_site_links'],true); //写
        $data['img'] = json_decode($data['web_site_logo'],true); //写
        return $this->response($data);
    }





    /**
     * @User: 刘海龙
     * @Date: 2021/3/17
     * @Time: 11:10
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 保存系统设置
     */
    public function saveSystemSetUp(){
        $id = $this->request->param('id',0);
        try {
            $validator = $this->validate($this->request->param(),
                \app\api\validate\System::rules($id),
                \app\api\validate\System::msg());
            if ($validator){
                $data = $this->request->param();
                $data['web_site_logo'] = json_encode($data['img']);
                $data['web_site_links'] = json_encode($data['moreNotifyObject']);
                $data['create_id'] = $this->request->uid;
                if (intval($id) === 0) {
                    SystemSetUp::create($data);
                } else {
                    $system = SystemSetUp::find($id);
                    $system->save($data);
                }
                return $this->response();
            }
        }catch (ValidateException $exception){
            return $this->response(403,$exception->getError());
        }
    }


}
