<?php
declare (strict_types=1);

namespace app\api\controller;

use app\api\helpers\traits\SearchDataForModel;
use app\api\model\Carousel;
use app\api\model\Navigation;
use think\exception\ValidateException;
use think\Request;

class Banner extends BaseController
{
    use SearchDataForModel;

    public function bannerList()
    {
        //获取每页显示的条数
        $limit = $this->request->param('limit');
        $map = [];
        if ($this->request->has('title') && !empty($this->request->param('title'))) {
            $map[] = ['title', 'like', '%' . $this->request->param('title') . '%'];
        }
        $data = $this->search(new Carousel(), $map, $limit, '', 'navigation');
        return $this->response($data);
    }

    /**
     * @User: 刘海龙
     * @Date: 2021/3/4
     * @Time: 14:36
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 保存Banner
     */
    public function saveBanner()
    {
        $id = $this->request->param('id', 0);
        try {
            $data = $this->request->param();
            $data['create_id'] = $this->request->uid;

            $data['status'] = $this->request->param('status', true) ? 1 : 0;
            $data['image'] = json_encode($data['image']);
            if (intval($id) === 0) {
                $carousel = Carousel::where('nav_id',$data['nav_id'])->find();
                if (!is_null($carousel)){
                    return $this->response(404, 'banner已存在,请不要重复添加');
                }
                Carousel::create($data);
            } else {
                $carousel = Carousel::find($id);
                $carousel->save($data);
            }
            return $this->response();
        } catch (ValidateException $exception) {
            return $this->response(403, $exception->getError());
        }
    }


    /**
     * @param $id
     * @User: 刘海龙
     * @Date: 2021/3/4
     * @Time: 15:09
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 获取信息
     */
    public function showBanner($id)
    {
        $data = Carousel::find($id)->toArray();
        if (is_null($data)) {
            return $this->response(404, '不存在');
        }
        $data['image'] = json_decode($data['image'], true);
        $data['status'] = $data['status'] == 1 ? true : false;
        return $this->response($data);
    }


    /**
     * @param $id
     * @User: 刘海龙
     * @Date: 2021/3/4
     * @Time: 15:34
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 删除banner
     */
    public function delBanner($id)
    {
        $ids = explode(',', $id);
        foreach ($ids as $id) {
            $carousel = Carousel::find($id);
            if (is_null($carousel)) {
                return $this->response(404, '不存在');
            }
            $carousel->delete();
        }
        return $this->response();
    }


    public function showBannerList(){
        $router = $this->request->param('router');
        $nav = Navigation::field('id,path')->where('path',$router)->find();

        if (is_null($nav)) {
            return $this->response(404, '导航不存在');
        }
        //根据路由获得id 获取banner
        $banner = Carousel::field('id,title,image,path,nav_id,keyword,desc')->where('nav_id',$nav->id)->find()->toArray();
        if (is_null($banner)) {
            return $this->response(404, 'banner不存在');
        }
        $banner['image'] = json_decode($banner['image'],true);
        return $this->response($banner);
    }
}
