<?php
declare (strict_types = 1);

namespace app\api\controller;

use app\api\helpers\traits\SearchDataForModel;
use app\api\model\Culture;
use app\api\model\Introduce;
use app\api\validate\PhotoAlbum;
use think\Exception;
use think\exception\ValidateException;
use think\Request;

class Company extends BaseController
{
    use SearchDataForModel;
    /**
     * 公司简介
     */

    /**
     * @User: 刘海龙
     * @Date: 2021/3/9
     * @Time: 14:37
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 获取唯一一条信息
     */
    public function IntroduceList(){
        $data = Introduce::find(1);
        $data['img'] = json_decode($data['ill_img']); //写
        $data['filePath'] = json_decode($data['ill_img']);//读
        $data['uploadList'] = json_decode($data['ability_img']); //写
        $data['qualifications'] = json_decode($data['ability_img']);//读
        return $this->response($data);
    }


    /**
     * @User: 刘海龙
     * @Date: 2021/3/9
     * @Time: 14:34
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 保存简介
     */
    public function saveIntroduce(){
        $id = $this->request->param('id',0);
        $data = $this->request->param();

        $data['create_id'] = $this->request->uid;
        $data['ill_img'] = json_encode($data['img']);
        $data['ability_img'] = json_encode($data['uploadList']);
        if (intval($id) === 0) {
            Introduce::create($data);
        } else {
            $introduce = Introduce::find($id);
            $introduce->save($data);
        }
        return $this->response();
    }



    /**
     * 公司文化
     */


    public function CultureList(){
        $data = Culture::find(1);
        $data['moreNotifyObject'] = json_decode($data['title_desc_arr'],true); //写
        return $this->response($data);
    }
    /**
     * @User: 刘海龙
     * @Date: 2021/3/9
     * @Time: 16:04
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 保存文化
     */
    public function saveCulture(){
        $id = $this->request->param('id',0);
        $data = $this->request->param();
        $data['title_desc_arr'] = json_encode($data['moreNotifyObject']);
        $data['create_id'] = $this->request->uid;
        if (intval($id) === 0) {
            Culture::create($data);
        } else {
            $culture = Culture::find($id);
            $culture->save($data);
        }
        return $this->response();
    }



    /**
     * 团队以及公司剪影
     */

    /**
     * @User: 刘海龙
     * @Date: 2021/3/9
     * @Time: 17:13
     * @return \think\response\Json
     * @throws \think\db\exception\DbException
     * 相册列表
     */
    public function PhotoList(){
        $limit = $this->request->param('limit');
        $map = [];
        if ($this->request->has('title') && !empty($this->request->param('title'))) {
            $map[] = ['title', 'like', '%' . $this->request->param('title') . '%'];
        }
        if ($this->request->has('column_id') && !empty($this->request->param('column_id'))) {
            $map[] = ['column_id', 'like', '%' . $this->request->param('column_id') . '%'];
        }
        $data = $this->search(new \app\api\model\PhotoAlbum(),$map,$limit,'','navigation')->each(function ($item,$key){
            $img_arr = json_decode($item['img'],true);
            $item['url']  = array_shift($img_arr) ?? '';
        });
        return $this->response($data);
    }


    /**\
     * @User: 刘海龙
     * @Date: 2021/3/9
     * @Time: 17:07
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 保存相册
     */
    public function savePhoto(){
        $id = $this->request->param('id',0);
        try {
            $validator = $this->validate($this->request->param(),
                PhotoAlbum::rules($id),
                PhotoAlbum::msg());
            if ($validator){
                $data = $this->request->param();
                $data['img'] = json_encode($data['img']);
                $data['create_id'] = $this->request->uid;
                $data['status'] = $this->request->param('status', true) ? 1 : 0;
                if (intval($id) === 0) {
                    \app\api\model\PhotoAlbum::create($data);
                } else {
                    $photo = \app\api\model\PhotoAlbum::find($id);
                    $photo->save($data);
                }
                return $this->response();
            }

        }catch (ValidateException $exception){
            return $this->response('403',$exception->getError());
        }
    }

    /**
     * @param $id
     * @User: 刘海龙
     * @Date: 2021/3/9
     * @Time: 17:16
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 获取唯一相册信息
     */
    public function showPhoto($id){
        $data = \app\api\model\PhotoAlbum::find($id)->toArray();
        if (is_null($data)) {
            return $this->response(404, '不存在');
        }
        $data['img'] = json_decode($data['img'], true);
        $data['status'] = $data['status'] == 1 ? true : false;
        return $this->response($data);
    }


    /**
     * @param $id
     * @User: 刘海龙
     * @Date: 2021/3/9
     * @Time: 17:19
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 删除相册
     */
    public function delPhoto($id){
        $ids = explode(',', $id);
        foreach ($ids as $id) {
            $photo =  \app\api\model\PhotoAlbum::find($id);
            if (is_null($photo)) {
                return $this->response(404, '该信息不存在');
            }
            $photo->delete();
        }
        return $this->response();
    }
}
