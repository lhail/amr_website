<?php
declare (strict_types = 1);

namespace app\api\controller;

use app\api\helpers\traits\SearchDataForModel;
use app\api\model\Assessment;
use app\api\model\CivilConfig;
use app\api\model\Civilization;
use app\api\model\Cooperative;
use app\api\model\Evaluation;
use app\api\model\Market;
use think\exception\ValidateException;
use think\Request;

class Server extends BaseController
{

    use SearchDataForModel;

    /**
     * 第三方测评
     */

    /**
     * @User: 刘海龙
     * @Date: 2021/3/10
     * @Time: 16:24
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 列表
     */
    public function assessList(){
        $data = Assessment::find(1);
        $data['img'] = json_decode($data['assess_img']); //写
        $data['filePath'] = json_decode($data['assess_img'],true) ?? '';//读
        $data['moreNotifyObject'] = json_decode($data['title_arr'],true) ?? ''; //写
        return $this->response($data);
    }
    /**
     * @User: 刘海龙
     * @Date: 2021/3/10
     * @Time: 16:21
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 保存第三方测评
     */
    public function saveAssess(){
        $id = $this->request->param('id',0);
        $data = $this->request->param();
        $data['assess_img'] = json_encode($data['img']); //写
        $data['title_arr'] = json_encode($data['moreNotifyObject']);
        $data['create_id'] = $this->request->uid;
        if (intval($id) === 0) {
            Assessment::create($data);
        } else {
            $assess = Assessment::find($id);
            $assess->save($data);
        }
        return $this->response();
    }
    /**
     * 服务质量
     */

    /**
     * @User: 刘海龙
     * @Date: 2021/3/10
     * @Time: 18:17
     * @return \think\response\Json
     * @throws \think\db\exception\DbException
     * 获取信息
     */
    public function evaluationList(){
        //获取每页显示的条数
        $limit = $this->request->param('limit');
        $map = [];
        if ($this->request->has('title') && !empty($this->request->param('title'))) {
            $map[] = ['title', 'like', '%' . $this->request->param('title') . '%'];
        }
        if ($this->request->has('currentValue') && !empty($this->request->param('currentValue'))) {
            $map[] = ['column_id', '=',$this->request->param('currentValue')];
        }
        $data = $this->search(new Evaluation(),$map,$limit)->each(function ($item,$key){
            $img_arr = json_decode($item['eval_img'],true);
            $item['url']  = array_shift($img_arr) ?? '';
            $item['column_name'] = Evaluation::$is_type[$item['column_id']];
            $item['moreNotifyObject'] = json_decode($item['title_arr'],true) ?? ''; //写
        });
        return $this->response($data);
    }


    /**
     * @param $id
     * @User: 刘海龙
     * @Date: 2021/3/10
     * @Time: 18:25
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 获取单一信息
     */
    public function showEvaluation($id){
        $data = Evaluation::find($id)->toArray();
        if (is_null($data)) {
            return $this->response(404, '不存在');
        }
        $data['img'] = json_decode($data['eval_img']); //写
        $data['moreNotifyObject'] = json_decode($data['title_arr'],true) ?? ''; //写
        $data['column_id'] = $data['column_id'].'';
        return $this->response($data);
    }


    /**
     * @User: 刘海龙
     * @Date: 2021/3/10
     * @Time: 17:51
     * @return \think\response\Json
     * 获取服务质量类别
     */
    public function evaluationTypeList(){
        $data = Evaluation::$is_type;
        return $this->response($data);
    }

    /**
     * @User: 刘海龙
     * @Date: 2021/3/10
     * @Time: 18:01
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 保存
     */
    public function saveEvaluation(){
        $id = $this->request->param('id',0);
        try {
            $validator = $this->validate($this->request->param(),
                \app\api\validate\Evaluation::rules($id),
                \app\api\validate\Evaluation::msg());
            if ($validator){
                $data = $this->request->param();
                $data['eval_img'] = json_encode($data['img']); //写
                $data['title_arr'] = json_encode($data['moreNotifyObject']);
                $data['create_id'] = $this->request->uid;
                if (intval($id) === 0) {
                    Evaluation::create($data);
                } else {
                    $eval = Evaluation::find($id);
                    $eval->save($data);
                }
                return $this->response();
            }
        }catch (ValidateException $exception){
            return $this->response(403, $exception->getError());
        }

    }


    /**
     * @param $id
     * @User: 刘海龙
     * @Date: 2021/3/10
     * @Time: 18:29
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 删除
     */
    public function delEvaluation($id){
        $ids = explode(',', $id);
        foreach ($ids as $id) {
            $eval =  Evaluation::find($id);
            if (is_null($eval)) {
                return $this->response(404, '该信息不存在');
            }
            $eval->delete();
        }
        return $this->response();
    }



    /**
     *文明眼管理平台
     */

    /**
     * @User: 刘海龙
     * @Date: 2021/3/11
     * @Time: 10:33
     * @return \think\response\Json
     * @throws \think\db\exception\DbException
     * 数据列表
     */
    public function CivilizationAdminList(){
        //获取每页显示的条数
        $limit = $this->request->param('limit');
        $map = [];
        if ($this->request->has('title') && !empty($this->request->param('title'))) {
            $map[] = ['title', 'like', '%' . $this->request->param('title') . '%'];
        }
        if ($this->request->has('currentValue') && !empty($this->request->param('currentValue'))) {
            $map[] = ['column_id', '=',$this->request->param('currentValue')];
        }
        $data = Civilization::with(['CivilConfig'])->where($map)->order('id','desc')->paginate($limit)->each(function ($item,$key){
            $img_arr = json_decode($item['civil_img'],true);
            $item['url']  = array_shift($img_arr) ?? '';
            $item['column_name'] = Civilization::$is_type[$item['column_id']];
        });
        return $this->response($data);
    }

    /**
     * @User: 刘海龙
     * @Date: 2021/3/11
     * @Time: 16:02
     * @return \think\response\Json
     * @throws \think\db\exception\DbException
     * 前端接口
     */
    public function CivilizationHomeList(){
        //获取每页显示的条数
        $limit = $this->request->param('limit');
        $map = [];
        if ($this->request->has('title') && !empty($this->request->param('title'))) {
            $map[] = ['title', 'like', '%' . $this->request->param('title') . '%'];
        }
        if ($this->request->has('currentValue') && !empty($this->request->param('currentValue'))) {
            $map[] = ['column_id', '=',$this->request->param('currentValue')];
        }
        $data = Civilization::with(['CivilConfig'=>function($query){
            $query->where('type_id',CivilConfig::TYPE_ONE);
            return $query;
        }])->where($map)->order('id','desc')->paginate($limit)->each(function ($item,$key){
            $img_arr = json_decode($item['civil_img'],true);
            $item['url']  = array_shift($img_arr) ?? '';
            $item['column_name'] = Civilization::$is_type[$item['column_id']];
            foreach ($item['CivilConfig'] as $k=>$v){
                $img_z_arr = json_decode($v['config_img'],true);
                $v['url']  = array_shift($img_z_arr) ?? '';
            }
        });
        return $this->response($data);
    }


    /**
     * @User: 刘海龙
     * @Date: 2021/3/11
     * @Time: 16:10
     * @return \think\response\Json
     * @throws \think\db\exception\DbException
     * 案例展示
     */
    public function getCaseList(){
        $limit = $this->request->param('limit',3);
        $map[] = ['type_id', '=',CivilConfig::TYPE_TWO];
        $data = $this->search(new \app\api\model\CivilConfig(),$map,$limit)->each(function ($item,$key){
            $img_arr = json_decode($item['config_img'],true);
            $item['url']  = array_shift($img_arr) ?? '';
        });
        return $this->response($data);
    }

    /**
     * @User: 刘海龙
     * @Date: 2021/3/11
     * @Time: 16:25
     * @return \think\response\Json
     * @throws \think\db\exception\DbException
     * 解决问题
     */
    public function solveTheProblem(){
        $limit = $this->request->param('limit',3);
        $map[] = ['type_id', '=',CivilConfig::TYPE_THREE];
        $data = $this->search(new \app\api\model\CivilConfig(),$map,$limit)->each(function ($item,$key){
            $img_arr = json_decode($item['config_img'],true);
            $item['url']  = array_shift($img_arr) ?? '';
        });
        return $this->response($data);
    }

    /**
     * @User: 刘海龙
     * @Date: 2021/3/11
     * @Time: 10:27
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 保存
     */
    public function saveCivilization(){
        $id = $this->request->param('id',0);
        try {
            $validator = $this->validate($this->request->param(),
                \app\api\validate\Civilization::rules($id),
                \app\api\validate\Civilization::msg());
            if ($validator){
                $data = $this->request->param();
                $data['civil_img'] = json_encode($data['img']); //写
                $data['create_id'] = $this->request->uid;
                if (intval($id) === 0) {
                    Civilization::create($data);
                } else {
                    $civili = Civilization::find($id);
                    $civili->save($data);
                }
                return $this->response();
            }
        }catch (ValidateException $exception){
            return $this->response(403, $exception->getError());
        }

    }

    /**
     * @param $id
     * @User: 刘海龙
     * @Date: 2021/3/11
     * @Time: 10:37
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 获取唯一信息
     */
    public function showCivilization($id){
        $data = Civilization::find($id)->toArray();
        if (is_null($data)) {
            return $this->response(404, '不存在');
        }
        $data['img'] = json_decode($data['civil_img']); //写
        $data['column_id'] = $data['column_id'].'';
        return $this->response($data);
    }

    /**
     * @param $id
     * @User: 刘海龙
     * @Date: 2021/3/16
     * @Time: 16:52
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 获取配置唯一信息
     */
    public function showCivilizationConfig($id){
        $data = CivilConfig::find($id)->toArray();
        if (is_null($data)) {
            return $this->response(404, '不存在');
        }
        $data['img'] = json_decode($data['config_img']); //写
        $data['type_id'] = $data['type_id'].'';
        return $this->response($data);
    }
    /**
     * @User: 刘海龙
     * @Date: 2021/3/11
     * @Time: 10:23
     * @return \think\response\Json
     * 获取类别
     */
    public function civilizationTypeList(){
        $data = Civilization::$is_type;
        return $this->response($data);
    }

    /**
     * @param $id
     * @User: 刘海龙
     * @Date: 2021/3/11
     * @Time: 10:39
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 删除信息
     */
    public function delCivil($id){
        $ids = explode(',', $id);
        foreach ($ids as $id) {
            $eval =  Civilization::find($id);
            if (is_null($eval)) {
                return $this->response(404, '该信息不存在');
            }
            $eval->delete();
        }
        return $this->response();
    }

    /**
     * @param $id
     * @User: 刘海龙
     * @Date: 2021/3/16
     * @Time: 16:53
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 删除配置
     */
    public function delCivilConfig($id){
        $ids = explode(',', $id);
        foreach ($ids as $id) {
            $eval =  CivilConfig::find($id);
            if (is_null($eval)) {
                return $this->response(404, '该信息不存在');
            }
            $eval->delete();
        }
        return $this->response();
    }
    /**
     * 保存文明眼配置
     */

    /**
     * @User: 刘海龙
     * @Date: 2021/3/11
     * @Time: 11:11
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 配置
     */
    public function saveCivilConfig(){
        $id = $this->request->param('id',0);
        try {
            $validator = $this->validate($this->request->param(),
                \app\api\validate\CivilConfig::rules($id),
                \app\api\validate\CivilConfig::msg());
            if ($validator){
                $data = $this->request->param();
                $data['config_img'] = json_encode($data['img']); //写
                $data['create_id'] = $this->request->uid;
                $data['status'] = $this->request->param('status', true) ? 1 : 0;
                if (intval($id) === 0) {
                    CivilConfig::create($data);
                } else {
                    $civil_config = CivilConfig::find($id);
                    $civil_config->save($data);
                }
                return $this->response();
            }
        }catch (ValidateException $exception){
            return $this->response(403, $exception->getError());
        }
    }

    /**
     * @User: 刘海龙
     * @Date: 2021/3/11
     * @Time: 15:49
     * @return \think\response\Json
     * 配置类别
     */
    public function civilConfigTypeList(){
        $data = CivilConfig::$is_type;
        return $this->response($data);
    }




    /**
     *市场调研
     */

    /**
     * @User: 刘海龙
     * @Date: 2021/3/11
     * @Time: 17:07
     * @return \think\response\Json
     * @throws \think\db\exception\DbException
     * 列表
     */
    public function MarketList(){
        //获取每页显示的条数
        $limit = $this->request->param('limit');
        $map = [];
        if ($this->request->has('title') && !empty($this->request->param('title'))) {
            $map[] = ['title', 'like', '%' . $this->request->param('title') . '%'];
        }
        if ($this->request->has('currentValue') && !empty($this->request->param('currentValue'))) {
            $map[] = ['column_id', '=',$this->request->param('currentValue')];
        }
        $data = $this->search(new Market(),$map,$limit)->each(function ($item,$key){

            $item['column_name'] = Market::$is_type[$item['column_id']];
        });
        return $this->response($data);
    }


    /**
     * @User: 刘海龙
     * @Date: 2021/3/11
     * @Time: 16:50
     * @return \think\response\Json
     * 市场调研类别
     */
    public function marketTypeList(){
        $data = Market::$is_type;
        return $this->response($data);
    }

    /**
     * @User: 刘海龙
     * @Date: 2021/3/11
     * @Time: 16:59
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 保存
     */
    public function saveMarket(){
        $id = $this->request->param('id',0);
        try {
            $validator = $this->validate($this->request->param(),
                \app\api\validate\Market::rules($id),
                \app\api\validate\Market::msg());
            if ($validator){
                $data = $this->request->param();
                $data['create_id'] = $this->request->uid;
                if (intval($id) === 0) {
                    Market::create($data);
                } else {
                    $market = Market::find($id);
                    $market->save($data);
                }
                return $this->response();
            }
        }catch (ValidateException $exception){
            return $this->response(403, $exception->getError());
        }
    }

    /**
     * @param $id
     * @User: 刘海龙
     * @Date: 2021/3/11
     * @Time: 17:00
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 获取唯一信息
     */
    public function showMarket($id){
        $data = Market::find($id)->toArray();
        if (is_null($data)) {
            return $this->response(404, '不存在');
        }
        $data['column_id'] = $data['column_id'].'';
        return $this->response($data);
    }

    /**
     * @param $id
     * @User: 刘海龙
     * @Date: 2021/3/11
     * @Time: 17:02
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 删除
     */
    public function delMarket($id){
        $ids = explode(',', $id);
        foreach ($ids as $id) {
            $market =  Market::find($id);
            if (is_null($market)) {
                return $this->response(404, '该信息不存在');
            }
            $market->delete();
        }
        return $this->response();
    }

}
