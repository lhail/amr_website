<?php
declare (strict_types = 1);

namespace app\api\controller;

use app\api\helpers\traits\SearchDataForModel;
use app\api\model\AuthRule;
use app\api\model\Navigation;
use app\api\validate\navigations;
use think\exception\ValidateException;
use think\Request;

class Nav extends BaseController
{
    use SearchDataForModel;


    /**
     * @User: 刘海龙
     * @Date: 2021/3/2
     * @Time: 10:16
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 数据列表
     */
    public function navList(){
        $data = Navigation::select();

        $name = $this->request->param('name', '');
        $type = $this->request->param('type', 'list');

        if (!empty($name) && $type == 'child') {
            $top = Navigation::where('parent_id', 0)
                ->where('title', 'like', '%' . $name . '%')
                ->select()->toArray();
        }

        if (!empty($top) && $type == 'child') {
            $res       = [];
            $parent_id = [];
            foreach ($top as $key => $val) {
                $res[]              = $val;
                $res[$key]['child'] = AuthRule::sortMenu($data, $val['id']);
                $parent_id[]        = $val['id'];
            }

            empty($name) ?:
                $down = Navigation::whereNotIn('parent_id', $parent_id)
                    ->where('title', 'like', '%' . $name . '%')
                    ->where('parent_id', '<>', 0)
                    ->select()->toArray();
            empty($down) ?: $res = array_merge($res, $down);
        } elseif (empty($name) && $type == 'child') {
            $data = $this->search(new Navigation(), [], $perPage = 200,'asc');
            $res  = AuthRule::sortMenu($data, 0);
        }elseif (empty($name) && $type == 'footer') {
            $map[] = array('parent_id','=',0);
            $res = $this->search(new Navigation(), $map, $perPage = 200,'asc');
        } else {
            $res = $this->search(new Navigation(), [], $perPage = 200,'asc');
        }
        return $this->response($res);
    }


    /**
     * @User: 刘海龙
     * @Date: 2021/3/2
     * @Time: 9:51
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 保存导航栏
     */
    public function saveNavigation(){
        $id = $this->request->param('id', 0);
        try {
            $validator = $this->validate($this->request->param(),
                navigations::rules($id),
                navigations::msg());
            if ($validator){
                $data = $this->request->param();
                $data['status'] = $this->request->param('status', true) ? 1 : 0;
                $data['create_id'] = $this->request->uid;
                $parent_id      = $this->request->param('parent_id', 0);
                if (intval($parent_id) === 0) {
                    $data['level'] = 0;
                } else {
                    $data['level'] = Navigation::find($parent_id)->level + 1;
                }
                if (intval($id) === 0) {
                    if (intval($parent_id) != 0) {
                        if (Navigation::find($parent_id)->level === 2) {
                            return $this->response(501, "菜单只能添加3级");
                        }
                    }
                    $nav = Navigation::where('path',$data['path'])->find();
                    if (!is_null($nav)){
                        return $this->response(404, '路由已存在,请不要重复添加');
                    }
                    Navigation::create($data);
                } else {
                    $nav = Navigation::find($id);
                    $nav->save($data);
                }
                return $this->response();
            }
        } catch (ValidateException $e) {
            // 验证失败 输出错误信息
            return $this->response(403, $e->getError());
        }

    }


    /**
     * @param $id
     * @User: 刘海龙
     * @Date: 2021/3/2
     * @Time: 10:40
     * @return \think\response\Json
     * 删除导航
     */
    public function delNav($id){
        $parent_id = Navigation::where('parent_id', $id)->count();
        if ($parent_id > 0) {
            return $this->response(501, '删除失败,此导航存在子导航');
        };
        Navigation::destroy($id);
        return $this->response();
    }
}
