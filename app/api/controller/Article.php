<?php
declare (strict_types = 1);

namespace app\api\controller;


use app\api\helpers\traits\ImageUpload;
use app\api\helpers\traits\SearchDataForModel;
use app\api\model\Articles;
use think\exception\ValidateException;

class Article extends BaseController
{
    use SearchDataForModel,ImageUpload;


    /**
     * @User: 刘海龙
     * @Date: 2021/3/4
     * @Time: 8:45
     * @return \think\response\Json
     * @throws \think\db\exception\DbException
     * 获取新闻列表
     */
    public function ArticleList(){
        //获取每页显示的条数
        $limit = $this->request->param('limit');
        $map = [];
        if ($this->request->has('title') && !empty($this->request->param('title'))) {
            $map[] = ['title', 'like', '%' . $this->request->param('title') . '%'];
        }
        if ($this->request->has('currentValue') && !empty($this->request->param('currentValue'))) {
            $map[] = ['column_id', '=',$this->request->param('currentValue')];
        }
        $data = $this->search(new Articles(),$map,$limit)->each(function ($item,$key){
            $item['column_name'] = Articles::$is_type[$item['column_id']];
        });
        return $this->response($data);
    }


    /**
     * @param $id
     * @User: 刘海龙
     * @Date: 2021/3/4
     * @Time: 9:38
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 根据id读取文章信息
     */
    public function ArticleShow($id){
        $data = Articles::find($id)->toArray();
        if (is_null($data)) {
            return $this->response(404, '此新闻不存在');
        }
        //拼接图片显示数组
        $data['img_show'] = [
            'name'=>$data['title'],
            'url'=>$data['img'],
            'uid'=>$data['id'],
        ];
        $data['status'] = $data['status'] == 1 ? true : false;
        $data['hot'] = $data['hot'] == 1 ? true : false;
        $data['top'] = $data['top'] == 1 ? true : false;
        $data['column_id'] = $data['column_id'].'';
       return $this->response($data);
    }


    /**
     * @User: 刘海龙
     * @Date: 2021/3/3
     * @Time: 16:41
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 新增新闻
     */
    public function saveArticle(){
        $id = $this->request->param('id',0);
        try {
            $validator = $this->validate($this->request->param(),
                \app\api\validate\Article::rules($id),
                \app\api\validate\Article::msg());
            if ($validator){
                $data = $this->request->param();
                $data['create_id'] = $this->request->uid;
                $data['hot'] = $this->request->param('hot', true) ? 1 : 0;
                $data['top'] = $this->request->param('top', true) ? 1 : 0;
                $data['status'] = $this->request->param('status', true) ? 1 : 0;
                if (intval($id) === 0) {
                    Articles::create($data);
                } else {
                    $article = Articles::find($id);
                    $article->save($data);
                }
                return $this->response();
            }
        }catch (ValidateException $exception) {
            return $this->response(403, $exception->getError());
        }
    }
    /**
     * @User: 刘海龙
     * @Date: 2021/3/3
     * @Time: 10:49
     * @return \think\response\Json
     * 获取新闻类别列表
     */
    public function articleTypeList(){
        $data = Articles::$is_type;
        return $this->response($data);
    }
    /**
     * @User: 刘海龙
     * @Date: 2021/3/3
     * @Time: 10:32
     * @return \think\response\Json
     * 上传图片
     */
    public function uploadImg(){
        if (!$this->request->file('file')) {
            return $this->response(501, '请选择上传文件');
        }
        $folder = $this->request->param('folder');
        $file   = $this->request->file('file');
        $result = $this->save($file, $folder);
        return $this->response($result);
    }

    /**
     * @User: 刘海龙
     * @Date: 2021/3/5
     * @Time: 10:49
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 根据类别重新读取新闻
     */
    public function homeArticleList(){
        $data = Articles::field('id,title,img,column_id,remark,create_time')->order('create_time desc')->select()->toArray();
        $newArr = [];
        foreach ($data as $key => $value) {
            $newArr[Articles::$is_column[$value['column_id']]][] = $value;
        }
        return $this->response($newArr);
    }


    /**
     * @param $id
     * @User: 刘海龙
     * @Date: 2021/3/4
     * @Time: 10:29
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 删除文章
     */
    public function delArticle($id){
        $ids = explode(',', $id);
        foreach ($ids as $id) {
            $article = Articles::find($id);
            if (is_null($article)) {
                return $this->response(404, '此新闻不存在');
            }
            $article->delete();
        }
        return $this->response();
    }


    /**
     * @param $id
     * @User: 刘海龙
     * @Date: 2021/3/12
     * @Time: 8:42
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 新闻详情
     */
    public function showArticle($id){
        $data = Articles::find($id)->toArray();
        if (is_null($data)) {
            return $this->response(404, '文章不存在');
        }
        $data['up'] = Articles::where('status !=0 AND id <' . $id)->order('id desc')->limit(1)->find();
        $data['down'] = Articles::where('status !=0 AND id >' . $id)->order('id')->limit(1)->find();
        return $this->response($data);
    }
}
