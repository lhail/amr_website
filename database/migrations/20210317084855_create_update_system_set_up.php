<?php

use think\migration\Migrator;
use think\migration\db\Column;

class CreateUpdateSystemSetUp extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('system_set_up', ['comment' => '系统设置']);
        $table->addColumn('web_site_title', 'string',array('limit' => 200,'null' => true,'comment' => '标题'))
            ->addColumn('web_site_email', 'string',array('limit' => 200,'null' => true,'comment' => '邮件'))
            ->save();
    }
}
