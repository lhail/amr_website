<?php

use think\migration\Migrator;
use think\migration\db\Column;

class CreateSystemSetUp extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('system_set_up', ['comment' => '系统设置']);
        $table->addColumn('web_site_logo', 'json',array('null' => true,'comment' => '网站logo'))
            ->addColumn('web_site_address', 'string',array('limit' => 180,'null' => true, 'comment' => '网站地址'))
            ->addColumn('web_site_hotline', 'string',array('limit' => 80,'null' => true, 'comment' => '服务热线'))
            ->addColumn('web_site_time_slot', 'string',array('limit' => 80,'null' => true, 'comment' => '服务热线时间段'))
            ->addColumn('web_site_icp', 'text',array('null' => true, 'comment' => '网站备案号'))
            ->addColumn('web_site_copy', 'string',array('limit' => 130,'null' => true, 'comment' => '版权信息'))
            ->addColumn('web_site_links', 'json',array('null' => true,'comment' => '友情链接json'))
            ->addColumn('create_id', 'integer',array('limit' => 30,'null' => true,'default' => 0, 'comment' => '创建人id'))
            ->addTimestamps()
            ->addSoftDelete()
            ->create();
    }
}
