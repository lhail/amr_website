<?php

use think\migration\Migrator;
use think\migration\db\Column;

class CreateCarousel extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('carousel', ['comment' => '轮播图']);
        $table->addColumn('title', 'string',array('limit' => 20, 'comment' => '标题'))
            ->addColumn('image', 'json',array('comment' => '轮播路径'))
            ->addColumn('path', 'string',array('limit' => 50, 'comment' => '路径'))
            ->addColumn('remark', 'string',array('limit' => 30, 'default' => '', 'comment' => '用户备注'))
            ->addColumn('status', 'boolean',array('limit' => 1, 'default' => 1, 'comment' => '导航标识(1启用 or 0禁用)'))
            ->addColumn('create_id', 'integer',array('limit' => 30, 'comment' => '创建人id'))
            ->addTimestamps()
            ->addSoftDelete()
            ->addIndex(array('title'))
            ->create();
    }
}
