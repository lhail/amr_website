<?php

use think\migration\Migrator;
use think\migration\db\Column;

class CreateModular extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('modular', ['comment' => '模块']);
        $table->addColumn('title', 'string',array('limit' => 50, 'comment' => '名称'))
            ->addColumn('icon', 'string',array('limit' => 120,'null' => true, 'comment' => '图标'))
            ->addColumn('path', 'string',array('limit' => 50,'null' => true, 'comment' => '路径'))
            ->addColumn('status', 'boolean',array('limit' => 1, 'default' => 1, 'comment' => '状态 是否禁用(1开启,0禁用)'))
            ->addColumn('remark', 'string',array('limit' => 30, 'null' => true, 'comment' => '文章备注'))
            ->addColumn('content', 'text',array('null' => true,'comment' => '内容'))
            ->addColumn('create_id', 'integer',array('limit' => 30,'default' => 0, 'comment' => '创建人id'))
            ->addTimestamps()
            ->addSoftDelete()
            ->addIndex(array('title'))
            ->create();
    }
}
