<?php

use think\migration\Migrator;
use think\migration\db\Column;

class CreateArticle extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('article', ['comment' => '文章表']);
        $table->addColumn('title', 'string',array('limit' => 50, 'comment' => '文章名称'))
            ->addColumn('sub_title', 'string',array('limit' => 80,'default' => '', 'comment' => '副标题'))
            ->addColumn('keyword', 'string',array('limit' => 15, 'default' => '', 'comment' => 'seo关键词'))
            ->addColumn('description', 'string',array('limit' => 25, 'default' => '', 'comment' => 'seo描述'))
            ->addColumn('author', 'string',array('limit' => 50, 'default' => '', 'comment' => '作者'))
            ->addColumn('source', 'string',array('limit' => 50, 'default' => '', 'comment' => '来源'))
            ->addColumn('column_id', 'integer',array('default' => 0, 'comment' => '栏目id'))
            ->addColumn('hot', 'integer',array('default' => 0, 'comment' => '是否热门文章(0普通文章,1热门文章)'))
            ->addColumn('top', 'integer',array('default' => 0, 'comment' => '是否置顶文章(0置顶文章,1置顶文章)'))
            ->addColumn('hits', 'integer',array('default' => 0, 'comment' => '点击量'))
            ->addColumn('img', 'string',array('limit' => 120,'default' => '', 'comment' => '封面'))
            ->addColumn('status', 'boolean',array('limit' => 1, 'default' => 1, 'comment' => '状态 是否禁用(1开启,0禁用)'))
            ->addColumn('remark', 'string',array('limit' => 30, 'null' => true, 'comment' => '文章备注'))
            ->addColumn('content', 'text',array('null' => true,'comment' => '内容'))
            ->addColumn('create_id', 'integer',array('limit' => 30, 'comment' => '创建人id'))
            ->addTimestamps()
            ->addSoftDelete()
            ->addIndex(array('title'))
            ->create();
    }
}
