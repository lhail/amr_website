<?php

use think\migration\Migrator;
use think\migration\db\Column;

class CreateRecruit extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     *  id:0,
    name:'',
    wages:'',
    requirement:'',
    address:'',
     */
    public function change()
    {
        $table = $this->table('recruit', ['comment' => '招聘']);
        $table->addColumn('name', 'string',array('limit' => 50, 'comment' => '名称'))
            ->addColumn('title_content', 'json',array('null' => true, 'comment' => '配置'))
            ->addColumn('wages', 'string',array('limit' => 80,'null' => true, 'comment' => '薪资'))
            ->addColumn('requirement', 'text',array('null' => true, 'comment' => '要求'))
            ->addColumn('duty', 'text',array('null' => true, 'comment' => '岗位职责'))
            ->addColumn('address', 'string',array('limit' => 100,'null' => true, 'comment' => '地址'))
            ->addColumn('content', 'text',array('null' => true,'comment' => '内容'))
            ->addColumn('create_id', 'integer',array('limit' => 30,'null' => true,'default' => 0, 'comment' => '创建人id'))
            ->addTimestamps()
            ->addSoftDelete()
            ->create();
    }
}
