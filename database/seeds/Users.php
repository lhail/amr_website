<?php

use think\migration\Seeder;

class Users extends Seeder
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = [
            [
                'username' => 'admin',
                'password' => md5(sha1(111111)),
                'mobile' => '18888888888',
                'head_img' => '/static/admin/images/a1.jpg',
                'email'=>'18888888888@139.com',
                'create_id'=>1,
                'remark'=>'优质出品Api',
                'last_login_time' => date('Y-m-d H:i:s'),
                'create_time' => date('Y-m-d H:i:s'),
                'update_time' => date('Y-m-d H:i:s'),
            ]
        ];

        $admin = $this->table('users');
        $admin->insert($data)
            ->save();
    }
}
